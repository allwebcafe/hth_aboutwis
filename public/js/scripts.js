$(document).ready(function() {
	// Pointer Events Polyfill
	PointerEventsPolyfill.initialize({});

	// Mouse over headshots	
	$('div#headshot').each(function() {
		var column = $(this).parent('#team-member').data('column');
		
		$(this).contenthover({
			data_selector:'#title',
		  effect:'slide',
		  slide_speed:500,
		  slide_direction:'top'
		});
		
	});
	
	// Show bio	
	$('.title').on('click', function() {

		var bio = $(this).parentsUntil('#team-member').next().next();
		var bioWidth = 424;
		var bioHeight = 519;
		var column = bio.parent().data('column');
		var row = bio.parent().data('row');
		var headshotP = bio.parent().position();
		var headshotW = 242;
		var headshotH = 288;
		
		$(this).parentsUntil('#headshot').children('.ch_element').children('.ch_normal').children('#headshot').children().addClass('current');
		
		$('#bio').each(function() {
			if ( !$(this).hasClass('hide') ) {
				$(this).slideToggle(500);
			}
		});
		
		$('div#headshot img').not('.current').each(function() {
			$(this).animate({opacity: .5});
		});
		
		$('.ghost').show();
			
		if ( column < 3 && row == 1 ) { // if 1x1 || 2x1
			bio.css('top', headshotP.top);
			bio.css('left', headshotP.left + headshotW - 2);
		} else if ( column > 2 && row == 1 ) { // if 3x1 || 4x1
			bio.css('top', headshotP.top);
			bio.css('left', headshotP.left - (headshotW * 2) );
		} else if ( column < 3 && row == 2 ) { // if 1x2 || 2x2
			bio.css('top', headshotP.top - headshotH);
			bio.css('left', headshotP.left + headshotW - 2);
		} else if ( column > 2 && row == 2 ) { // if 3x2 || 4x2
			bio.css('top', headshotP.top - headshotH);
			bio.css('left', headshotP.left - (headshotW * 2) );
		}
		
		bio.height(bioHeight);
		bio.width(bioWidth);
		bio.slideToggle(500);
		
	});
	
	// Hide Bios
	$('#bio #hide-bio').on('click', function() {
		$('div#headshot img').each(function() {
			$(this).animate({opacity: 1});
			$(this).removeClass('current');
		});
		$(this).parent().slideToggle(500);

		$('.ghost').hide();
	});
		

	// Contact form 
	$(function(){
		var form  = $('#ajax_form');
		form.submit(function(e){
	    $.post( form.attr('action'), form.serialize(), function(data) {
      	if (data.success == false) {
        	//data.errors
          $.each(data.errors, function(i, item){
          	var input = $('[name="' + i + '"]');
          	var error = ($.isArray(item) ? item.join('<br/>') : item);
          	input.addClass('error_message');
          });
				} else if (data.success) {
        	$('#contact-form').find("input[type=text], textarea").val("");
        	$('#success-form').fadeIn('fast');
        }
			});
	
	    e.preventDefault();
	    return false;
	  });
	});
	
	
	// Parallax Effects
	function parallax(){
		var scrolled = $(window).scrollTop();
		
		$('#hero-block').css('background-position', '-1200px ' + -(800 + scrolled * 0.2) + 'px');
		
		var rightBlock = $('#hero-block .content-container .right-block');
		rightBlock.css('margin-top', (310 - scrolled * 0.4) + 'px');

		//console.log(scrolled);
		if ( scrolled > 1850 && scrolled < 2350 ) {
			var add = 1850 - scrolled;
			$('#quote').css('background-position', 'right ' + -(150 - add * 0.3) + 'px');
			$('#quote .content-container').css('margin-top', (-1 + add * 0.3) + 'px');
		}		

	}
	
	$(window).scroll(function(e){
    parallax();
	});
	
	
	
	
	// Because Placeholders don't work in >=IE9 and I apparently don't know how to use modernizr properly
	(function ($) {
    $.fn.placehold = function (placeholderClassName) {
        var placeholderClassName = placeholderClassName || "placeholder",
            supported = $.fn.placehold.is_supported();

        function toggle() {
            for (i = 0; i < arguments.length; i++) {
                arguments[i].toggle();
            }
        }

        return supported ? this : this.each(function () {
            var $elem = $(this),
                placeholder_attr = $elem.attr("placeholder");

            if (placeholder_attr) {
                if ($elem.val() === "" || $elem.val() == placeholder_attr) {
                    $elem.addClass(placeholderClassName).val(placeholder_attr);
                }

                if ($elem.is(":password")) {
                    var $pwd_shiv = $("<input />", {
                        "class": $elem.attr("class") + " " + placeholderClassName,
                        "value": placeholder_attr
                    });

                    $pwd_shiv.bind("focus.placehold", function () {
                        toggle($elem, $pwd_shiv);
                        $elem.focus();
                    });

                    $elem.bind("blur.placehold", function () {
                        if ($elem.val() === "") {
                            toggle($elem, $pwd_shiv);
                        }
                    });

                    $elem.hide().after($pwd_shiv);
                }

                $elem.bind({
                    "focus.placehold": function () {
                        if ($elem.val() == placeholder_attr) {
                            $elem.removeClass(placeholderClassName).val("");
                        }
                    },
                    "blur.placehold": function () {
                        if ($elem.val() === "") {
                            $elem.addClass(placeholderClassName).val(placeholder_attr);
                        }
                    }
                });

                $elem.closest("form").bind("submit.placehold", function () {
                    if ($elem.val() == placeholder_attr) {
                        $elem.val("");
                    }

                    return true;
                });
            }
        });
    };

    $.fn.placehold.is_supported = function () {
        return "placeholder" in document.createElement("input");
    };
	})(jQuery);
	
	$("input#first_name").placehold("First Name");
	$("input#last_name").placehold("Last Name");
	$("input#email_address").placehold("Email");
	$("input#phone_number").placehold("Phone Number");
	$("textarea#user_message").placehold("Message");
	
	
});